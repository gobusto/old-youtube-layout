Old Youtube Layout
==================

This is an add-on for Mozilla Firefox, which simply adds `disable_polymer=1` to
the end of Youtube video URLs.

Get it here: <https://addons.mozilla.org/en-US/firefox/addon/old-youtube-layout>

Purpose
-------

Someone online wanted to get the old Youtube layout back, and it turns out that
you can do that by appending `&disable_polymer=1` to the end of a video URL:

<https://www.youtube.com/watch?v=D-UmfqFjpl0>

<https://www.youtube.com/watch?v=D-UmfqFjpl0&disable_polymer=1>

Making a browser extension to do this automatically didn't sound too hard, so I
used it as an opportunity to learn about building WebExtensions-based add-ons.

Bugs
----

It doesn't work if you navigate to a video from another page, such as a channel
or the search results (unless you open it in a new tab), due to the way Youtube
handles going from one page to another. It does work if you go from one _video_
to another, though!

I'm not sure how to fix this, but reloading (pressing F5) is a work-around.

Licensing
---------

Copyright (c) 2018 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
