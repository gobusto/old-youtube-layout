function redirect(request) {
  // If we've already modified the URL, don't do anything:
  if (request.url.indexOf("&disable_polymer=") >= 0) { return {}; }
  // Otherwise, add "disable_polymer=1" to the end:
  return { redirectUrl: request.url + "&disable_polymer=1" };
}

// Catch any youtube page loads before they start:
browser.webRequest.onBeforeRequest.addListener(
  redirect,
  { urls: ["https://www.youtube.com/watch?v=*"] },
  ["blocking"]
);
